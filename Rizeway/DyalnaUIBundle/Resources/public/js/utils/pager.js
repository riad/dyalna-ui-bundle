var RizewayPager = function(dom) {

    var dom_object = dom;

    $(dom_object).click(function(e){
        e.preventDefault();
        var pager = $(this);
        pager.addClass('selected');
        $.ajax({
            url: pager.attr('href'),
            type: 'GET',
            success: function(data) {
                $('<div>'+data+'</div>').insertAfter(pager);
                pager.replaceWith('<div class="rizeway-separator"></div>');
            }
        });
    });

};