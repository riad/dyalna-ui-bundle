function SwitchLocale(dom) {
    $('a.current', dom).click(function(e) {
        e.preventDefault();
        if ($(this).hasClass('opened')) {
            $(this).removeClass('opened');
            $('a:not(.current)', dom).css('display', 'none');
        } else {
            $(this).addClass('opened');
            $('a:not(.current)', dom).css('display', 'block');
        }
    })
}